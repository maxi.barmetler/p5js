const size = {
  w: 600,
  h: 343
}

function setup() {
  createCanvas(size.w, size.h)
  for (let y of range(size.h)) {
    for (let x of range(size.w)) {
      re = x / size.w * 3.5 - 2.5
      im = y / size.h * 2 - 1
      let z = new Complex(0)
      let c = new Complex(re, im)
      let iterations = 0
      for (let i of Array(1000).keys()) {
        z = z.mul(z).add(c)
        if (z.abs() > 4) {
          iterations = i
          break
        }
      }
      let col = color(`hsl(${(iterations * 15) % 360}, 100%, 50%)`)
      if (iterations == 0) {
        col = color(0)
      }
      set(x, y, col)
    }
  }
  updatePixels()
}

function draw() { }

function range(toex, from = 0) {
  return [...Array(toex - from).keys()].map(i => i + from)
}
