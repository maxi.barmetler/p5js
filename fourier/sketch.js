let time = 0
let points = []
let slider

function setup() {
  createCanvas(600, 400)
  slider = createSlider(1, 150, 100)
}

function draw() {
  background(35, 30, 40)
  translate(140, 200)

  strokeWeight(0)
  fill(30, 27, 34)
  rect(-130, -150, 260, 300)

  strokeWeight(0)
  fill(30, 27, 34)
  rect(145, -150, 305, 300)

  strokeWeight(1)
  for (let i = 0; i < 7; ++i) {
    let y = -75 + i * 25
    stroke(i % 2 == 1 ? 40 : 30)
    line(150, y, 445, y)
  }

  stroke(50)
  strokeWeight(2)
  line(150, 100, 445, 100)
  line(150, -100, 445, -100)
  line(160, -110, 160, 110)

  let base_radius = 100
  let steps = 4

  stroke(2)

  let x = 0
  let y = 0

  for (let i = 0; i < slider.value(); ++i) {
    let angle = time * (1 + 2 * i)

    noFill()
    stroke(50)
    strokeWeight(1)
    let radius = base_radius / (1 + 2 * i)
    ellipse(x, y, 2 * radius)
    let new_x = x + radius * cos(angle)
    let new_y = y + radius * sin(angle)
    stroke(200)
    strokeWeight(2)
    line(x, y, new_x, new_y)
    x = new_x
    y = new_y
    fill(200)
    strokeWeight(1)
    ellipse(x, y, 6)
  }

  points.unshift(y)

  stroke(100)
  strokeWeight(1)
  line(x, y, 160, y)
  stroke(0, 0, 0, 0)
  ellipse(x, y, 4)

  stroke(200)
  strokeWeight(2)
  noFill()
  beginShape()
  for (let i = 0; i < points.length; ++i) {
    vertex(160 + 0.5 * i, points[i])
  }
  endShape()

  if (points.length > 560) {
    points.pop()
  }

  time += 0.01
}