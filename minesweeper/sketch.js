const HIDDEN = 0
const UNCOVERED = 1
const FLAGGED = 2
const PLAYING = 0
const WON = 1
const LOST = 2

let size = {
  w: 10,
  h: 8
}
let cSize = 64

let gamestate

let field
let totalBombs
let uncoveredFields
let totalFlags
let fieldcreated

let slider
let resetButton
let wInput
let hInput

function setup() {
  createCanvas(size.w * cSize + 32, size.h * cSize + 32 + 64 + 16)
  textAlign(CENTER, CENTER)

  slider = createSlider(0, 99, 20)
  slider.position(20, 48);

  wInput = createInput('10')
  wInput.size(64, 20)
  wInput.position(180, 22)
  hInput = createInput('8')
  hInput.size(64, 20)
  hInput.position(180, 48)

  resetButton = createButton('RESTART')
  resetButton.size(128, 54)
  resetButton.position((size.w * cSize + 32 - resetButton.width) / 2, 48 - (resetButton.height / 2))
  resetButton.mousePressed(reset)

  reset()

  window.oncontextmenu = function () {
    this.mouseClicked(false)
    return false
  }
}

function draw() {
  background(40, 35, 36)
  translate(16, 16)
  textSize(14);

  // background for top panel
  strokeWeight(0)
  fill(35, 31, 32)
  rect(0, 0, cSize * size.w, 64)

  // Mines percent info
  fill(150)
  text(`Mines Percent: ${slider.value()}%`, slider.width / 2 + 20 - 16, 20)

  // Labels for w and h
  text('w', 155, 18)
  text('h', 155, 64 - 18)

  // background for mine count
  fill(150)
  translate(size.w * cSize - 5, 0)
  rect(-100, 5, 100, 54)
  fill(30)
  textSize(24);
  text(`${totalBombs - totalFlags}`, -50, 32)
  textSize(14);
  translate(-size.w * cSize + 5, 0)

  // Play area
  // background for tiles
  translate(0, 64 + 16)
  strokeWeight(1)
  stroke(150)
  fill(180)
  for (let y of range(size.h)) {
    for (let x of range(size.w)) {
      rect(x * cSize, y * cSize, cSize, cSize)
    }
  }

  stroke(0)
  for (let y of range(size.h)) {
    for (let x of range(size.w)) {
      fill(240)
      strokeWeight(16 / cSize)
      translate(x * cSize, y * cSize)
      scale(cSize / 16)
      translate(8, 8)
      if (field[y][x].state == UNCOVERED) {
        // draw mine or count
        if (field[y][x].bomb) {
          fill(40, 38, 45)
          strokeWeight(16 / cSize)
          for (let i = 0; i < 10; i++) {
            ellipse(0, 0, 12, 1);
            rotate(PI / 5);
          }
          ellipse(0, 0, 8)
          strokeWeight(0)
          for (let i of range(4)) {
            fill(42 + 4 * i, 40 + 2 * i, 47 + 2 * i)
            ellipse(0, 0, 7 - i)
          }
          strokeWeight(16 / cSize)
        } else {
          strokeWeight(0)
          n = field[y][x].neighbors
          if (n > 0) {
            fill(['blue', 'green', 'red', 'purple', 'black', 'maroon', 'gray', 'turquoise'][n - 1])
            text(n, 0, 0)
          }
        }
      } else {
        // draw tile
        rect(-8, -8, 16, 16)
      }
      // draw flags
      if (field[y][x].state == FLAGGED) {
        fill(200, 30, 28)
        strokeWeight(16 / cSize)
        beginShape()
        vertex(-3, 6)
        vertex(-3, -6)
        vertex(4, -3)
        vertex(-2, 0)
        vertex(-2, 6)
        endShape(CLOSE)
      }
      translate(-8, -8)
      scale(16 / cSize)
      translate(-x * cSize, -y * cSize)
    }
  }

  // draw frames
  stroke(50)
  strokeWeight(4)
  noFill()
  rect(0, -64 - 16, size.w * cSize, 64)
  rect(0, 0, size.w * cSize, size.h * cSize)

  // Draw win or lose
  textSize(size.w * cSize / 7)
  if (gamestate == LOST) {
    fill(100, 0, 0, 100)
    stroke(30, 0, 0)
    rect(0, 0, size.w * cSize, size.h * cSize)
    fill(150, 0, 0)
    text('GAME OVER', size.w * cSize / 2, size.h * cSize / 2)
  } else if (gamestate == WON) {
    fill(10, 100, 50, 100)
    stroke(0, 50, 30)
    rect(0, 0, size.w * cSize, size.h * cSize)
    fill(20, 200, 100)
    text('YOU WON!', size.w * cSize / 2, size.h * cSize / 2)
  }
}

function mouseClicked(left = true) {
  if (gamestate != PLAYING) {
    return
  }
  let x = floor((mouseX - 16) / cSize)
  let y = floor((mouseY - 32 - 64) / cSize)
  if (inField(x, y)) {
    if (field[y][x].state == UNCOVERED) {
      // TODO uncover neighbors
      // Count neighboring flags
      let flags = 0
      for (let _y of range(y + 2, y - 1)) {
        for (let _x of range(x + 2, x - 1)) {
          if (inField(_x, _y)) {
            if (field[_y][_x].state == FLAGGED) {
              ++flags
            }
          }
        }
      }
      if (flags >= field[y][x].neighbors) {
        for (let _y of range(y + 2, y - 1)) {
          for (let _x of range(x + 2, x - 1)) {
            uncover(_x, _y)
          }
        }
      }

    } else if (!left) {
      if (field[y][x].state == FLAGGED) {
        field[y][x].state = HIDDEN
        --totalFlags
      } else {
        field[y][x].state = FLAGGED
        ++totalFlags
      }
    } else if (field[y][x].state == HIDDEN) {
      // create field if it isn't yet
      if (!fieldcreated) {
        createField(x, y)
      }
      uncover(x, y)
    }
  }
}

function uncover(x, y) {
  if (!inField(x, y) || field[y][x].state != HIDDEN) {
    return
  }
  field[y][x].state = UNCOVERED
  ++uncoveredFields
  // Check for lose
  if (field[y][x].bomb) {
    gamestate = LOST
    return
  }
  // Check for win
  if (uncoveredFields >= size.w * size.h - totalBombs) {
    gamestate = WON
    return
  }

  if (field[y][x].neighbors == 0) {
    for (let _y of range(y + 2, y - 1)) {
      for (let _x of range(x + 2, x - 1)) {
        if (_x != x || _y != y) {
          uncover(_x, _y)
        }
      }
    }
  }
}

function reset() {
  size = {
    w: max(1, parseInt(wInput.value())),
    h: max(1, parseInt(hInput.value())),
  }
  cSize = 640 / size.w
  createCanvas(size.w * cSize + 32, size.h * cSize + 32 + 64 + 16)
  resetButton.position(max(272, (size.w * cSize + 32 - resetButton.width) / 2), 48 - (resetButton.height / 2))

  field = []
  for (let y of range(size.h)) {
    field[y] = []
    for (let x of range(size.w)) {
      field[y][x] = {
        state: HIDDEN,
        neighbors: 0,
        bomb: false
      }
    }
  }
  totalBombs = (int)(size.w * size.h * slider.value() / 100)
  if (totalBombs >= size.w * size.h) {
    totalBombs = size.w * size.h - 1
  }
  totalFlags = 0
  uncoveredFields = 0
  fieldcreated = false
  gamestate = PLAYING
}

function createField(_x, _y) {
  let createdBombs = 0
  while (createdBombs < totalBombs) {
    x = floor(random() * size.w)
    y = floor(random() * size.h)
    if (!field[y][x].bomb && (x != _x || y != _y)) {
      field[y][x].bomb = true
      ++createdBombs
    }
  }

  refreshCounts(0, 0, size.w, size.h)
  fieldcreated = true
}

function refreshCounts(fromx, fromy, tox, toy) {
  for (let y of range(toy, fromy)) {
    for (let x of range(tox, fromx)) {
      neighbors = 0
      for (let _y of range(y + 2, y - 1)) {
        for (let _x of range(x + 2, x - 1)) {
          if (inField(_x, _y)) {
            if (field[_y][_x].bomb) {
              ++neighbors
            }
          }
        }
      }
      field[y][x].neighbors = neighbors
    }
  }
}

function range(toex, from = 0) {
  return [...Array(toex - from).keys()].map(i => i + from)
}

function inField(x, y) {
  return x >= 0 && x < size.w && y >= 0 && y < size.h
}
